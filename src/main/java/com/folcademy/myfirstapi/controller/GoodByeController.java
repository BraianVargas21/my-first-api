package com.folcademy.myfirstapi.controller;


import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;D
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GoodByeController {
    @PostMapping("/goodbye")
    public String goodbye(){
        return "goodbye";
    }
}
